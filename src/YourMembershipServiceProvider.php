<?php

namespace Codesmen\YourMembershipLumenAPI;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class YourMembershipServiceProvider extends ServiceProvider
{
    protected $defer = true;

    private $name = 'yourmembership-lumen-api';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            sprintf('%s/config/%s.php', __DIR__, $this->name) => config_path(sprintf('%s.php', $this->name)),
        ]);

        Response::macro('xml', function (\SimpleXMLElement $value) {
            return Response::make($value->asXML())
                ->header('Content-type', 'text/xml');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $that = $this;

        $this->mergeConfigFrom(
            sprintf('%s/config/%s.php', __DIR__, $this->name), $this->name
        );

        $this->app->bind(\Codesmen\YourMembershipLumenAPI\YMLA::class, function ($app) use ($that) {
            return new \Codesmen\YourMembershipLumenAPI\YMLA(
                app(\GuzzleHttp\Client::class),
                app(\Illuminate\Contracts\Cache\Repository::class),
                app(\Illuminate\Http\Request::class),
                env('YM_API_KEY'),
                env('YM_SECRET_API_KEY'),
                env('YM_SA_PASSCODE')
            );
        });
    }

    public function provides()
    {
        return [\Codesmen\YourMembershipLumenAPI\YMLA::class];
    }
}
